package io.paybywallet.poc;

// create a completely new and unique pair of keys.
// see more about KeyPair objects:
// https://stellar.github.io/java-stellar-sdk/org/stellar/sdk/KeyPair.html
import org.stellar.sdk.KeyPair;

public class KeyPairGenerator {
    public static KeyPair generateRandomKeyPair() {
        KeyPair pair = KeyPair.random();
        System.out.println("Seed:" + new String(pair.getSecretSeed()));
        System.out.println("AccountID:" + pair.getAccountId());
        return pair;
    }
}