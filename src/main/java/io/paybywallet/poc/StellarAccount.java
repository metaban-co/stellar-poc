package io.paybywallet.poc;

import java.net.*;
import java.io.*;
import java.util.*;
import org.stellar.sdk.*;
import org.stellar.sdk.responses.AccountResponse;

public class StellarAccount {
    
    public String createAccount() throws IOException {
        KeyPair pair = KeyPairGenerator.generateRandomKeyPair();
        String friendbotUrl = String.format("https://friendbot.stellar.org/?addr=%s", pair.getAccountId());

        System.out.println("Calling friendbot...");
        InputStream response = new URL(friendbotUrl).openStream();
        String body = new Scanner(response, "UTF-8").useDelimiter("\\A").next();
        System.out.println("SUCCESS! You have a new account :)\n" + body);
        return pair.getAccountId();
    }

    public void getAccountDetails(String accountId) throws IOException {
        KeyPair pair = KeyPair.fromAccountId(accountId);
        Server server = new Server("https://horizon-testnet.stellar.org");
        AccountResponse account = server.accounts().account(pair.getAccountId());
        System.out.println("Balances for account " + pair.getAccountId());
        for (AccountResponse.Balance balance : account.getBalances()) {
            System.out.printf("Type: %s, Code: %s, Balance: %s%n", balance.getAssetType(), balance.getAssetCode(),
                    balance.getBalance());
        }
    }
}
