package io.paybywallet.poc;

public class App {
    public static void main(String[] args) {
        StellarAccount account = new StellarAccount();
        try {
            String accountId = account.createAccount();
            account.getAccountDetails(accountId);

            StellarPayment payment = new StellarPayment();
            payment.makeTransfer(accountId, "GCIJPSI66UZUAPOP637DQMTJBGNLW4EBNKAQKLCL2ZRROPKFACNSKZRM");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
