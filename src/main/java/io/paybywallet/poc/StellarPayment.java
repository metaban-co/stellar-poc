package io.paybywallet.poc;

import java.io.IOException;

import org.stellar.sdk.*;
import org.stellar.sdk.responses.AccountResponse;
import org.stellar.sdk.responses.SubmitTransactionResponse;

public class StellarPayment {

    public void makeTransfer(String sourceAddress, String destinationAddress) throws IOException {

        Server server = new Server("https://horizon-testnet.stellar.org");

        KeyPair source = KeyPair.fromSecretSeed(sourceAddress);
        KeyPair destination = KeyPair.fromAccountId(destinationAddress);

        // First, check to make sure that the destination account exists.
        // You could skip this, but if the account does not exist, you will be charged
        // the transaction fee when the transaction fails.
        // It will throw HttpResponseException if account does not exist or there was another error.
        server.accounts().account(destination.getAccountId());

        // If there was no error, load up-to-date information on your account.
        AccountResponse sourceAccount = server.accounts().account(source.getAccountId());

        // Start building the transaction.
        Transaction transaction = new Transaction.Builder(sourceAccount, Network.TESTNET)
                .addOperation(new PaymentOperation.Builder(destination.getAccountId(), new AssetTypeNative(), "10000").build())
                // A memo allows you to add your own metadata to a transaction. It's
                // optional and does not affect how Stellar treats the transaction.
                .addMemo(Memo.text("1336561188"))
                // Wait a maximum of three minutes for the transaction
                .setTimeout(180)
                // Set the amount of lumens you're willing to pay per operation to submit your transaction
                .setBaseFee(Transaction.MIN_BASE_FEE)
                .build();
        // Sign the transaction to prove you are actually the person sending it.
        transaction.sign(source);

        // And finally, send it off to Stellar!
        try {
            SubmitTransactionResponse response = server.submitTransaction(transaction);
            System.out.println("Success!");
            System.out.println(response);
        } catch (Exception e) {
            System.out.println("Something went wrong!");
            System.out.println(e.getMessage());
            // If the result is unknown (no response body, timeout etc.) we simply resubmit
            // already built transaction:
            // SubmitTransactionResponse response = server.submitTransaction(transaction);
        }
    }
}
