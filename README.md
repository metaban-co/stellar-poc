# Pay By Wallet

## Proof of Concept

We want to do the following tests:

1. Create account in stellar network [done]

2. Transfer ETH from testnet wallet to Stellar Wallet (Merchant)

3. Exchange ETH into USDC tokens

4. Execute Smart Contract

---
### Important Notes

1. Stellar requires accounts to hold a minimum balance of 1 XLM before they actually exist. Until it gets a bit of funding, your keypair doesn’t warrant space on the ledger.

    - For production use check the [lumen buying guide](https://www.stellar.org/lumens/exchanges).

1. Lorem
---
PBW© 2021